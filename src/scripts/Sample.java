package scripts;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test (suiteName="Sample")
public class Sample {

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Do Before");
	}
	
    @Test (testName="compareStrings")
    public void compareStrings() {
        AssertJUnit.assertEquals("First Line", "First Line");
    }
    
    @AfterMethod
    public void afterMethod() {
		System.out.println("Do After");
	}
    
}
